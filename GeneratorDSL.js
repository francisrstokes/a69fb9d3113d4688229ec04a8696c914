const getDSLValue = (iterator, last) => {
  const {value, done} = iterator.next(last);
  if (done) {
    return value;
  }

  switch (value) {
    case 'sword': {
      return getDSLValue(iterator, {
        weaponType: "Shiny Sword",
        damage: 10
      });
    }
    case 'sponge': {
      return getDSLValue(iterator, {
        weaponType: "Greasy Sponge",
        damage: 1
      });
    }
    default: {
      throw new Error('unrecongised command in DSL');
    }
  }
}
const dsl = gen => getDSLValue(gen());

const finalWeapon = dsl(function*() {
  const w1 = yield 'sword';
  const w2 = yield 'sponge';
  console.log(`You pick up a ${w1.weaponType} and a ${w2.weaponType}!`);

  return w1;
});

console.log(finalWeapon);
// -> You pick up a Shiny Sword and a Greasy Sponge!
// -> { weaponType: "Shiny Sword", damage: 10 }